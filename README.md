# Programmation Parallèle TD 4.2

http://info.univ-angers.fr/pub/richer/ensm2_para_td4.php

Utiliser MPI et 3 instances du même programme pour trier un tableau d'entiers

La première instance, de rang 0, est le master : il crée le tableau initial de N éléments et le divise en 3 parties. Il réparti les 2 autres parties du tableau sur les instances slave 1 et 2

Chaque esclave trie le sous tableau de taille N/3 qui lui a été transmis par le master, puis

* l'esclave 2 transmet son tableau à l'esclave 1 pour réaliser un merge (fusion)
* l'esclave 1 réalise le merge et transmet son tableau de taille 2.N/3 au marter
* enfin le master réalise un merge de son tableau trié de taille N/3 et du tableau transmis par l'esclave 1 de taille 2.N/3
