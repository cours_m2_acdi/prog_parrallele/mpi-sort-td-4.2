#include <iostream>
#include <algorithm>
#include "mmpi/mmpi.h"
#include "utils/repartition_algorithm.h"
#include "utils/utils.h"
#include "utils/ansi_colors.h"
#include "merge.h"
#include "cputimer/cpu_timer.h"

void scatterArray(MMPI &mmpi, int *array, std::vector<ThreadPosition> &positions);

int main(int argc, char * argv[]) {
    CPUTimer cpuTimer;

    MPI::Init(argc, argv);
    MMPI mmpi;

    cpuTimer.start();

    int * local_data = nullptr;
    int * global_data = nullptr;

    int global_data_size = 14;
    int local_data_size;
    std::vector<ThreadPosition> positions(3);


    if(mmpi.is_master()){

        int opt;

        while ((opt = getopt(argc, argv, "s:")) != -1) {
            switch (opt) {
                case 's':
                    global_data_size = atoi(optarg);
                    break;
                default: /* aq?aq */
                    fprintf(stderr, "Usage: %s [-s size]",
                            argv[0]);
                    exit(EXIT_FAILURE);
            }
        }


        global_data = new int[global_data_size];

        populateArrayRandom(global_data, global_data_size);

        //Compute repartition of the array
        equalRepartition(positions, global_data_size, mmpi.max_cpus());

        mmpi << "Repartition : ";
        for(auto p : positions){
            mmpi << p.size << ", ";
        }
        mmpi << MMPI::endl;

        scatterArray(mmpi, global_data, positions);

        local_data_size = positions[0].size;
        local_data = new int[local_data_size];
        memcpy(local_data, global_data, sizeof(int) * static_cast<size_t>(local_data_size));

        std::sort(local_data, local_data + local_data_size);

    }else if(mmpi.cpu_rank() == 1 || mmpi.cpu_rank() == 2){
        mmpi.remote(0);

        mmpi.recv(local_data_size);
        // each processor creates its local data
        local_data = new int[local_data_size];

        mmpi.recv(local_data, local_data_size);

        std::sort(local_data, local_data + local_data_size);

//        logArray(local_data, local_data_size, mmpi, "local_data=", "");
    }
    MPI::COMM_WORLD.Barrier();

    if(mmpi.cpu_rank() == 2){
        mmpi.remote(1);
        mmpi.send(local_data_size);
        mmpi.send(local_data, local_data_size);

        delete[] local_data;
    }else if(mmpi.cpu_rank() == 1){
        mmpi.remote(2);

        int receivedArraySize;
        int * receivedArray;
        mmpi.recv(receivedArraySize);
        receivedArray = new int[receivedArraySize];

        mmpi.recv(receivedArray, receivedArraySize);

        int * dest = new int[local_data_size + receivedArraySize];

        merge(local_data, local_data_size, receivedArray, receivedArraySize, dest);
        int finalSize = local_data_size + receivedArraySize;
//        logArray(dest, finalSize, mmpi, "merge_1=", "");

        mmpi.remote(0);
        mmpi.send(finalSize);
        mmpi.send(dest, finalSize);

        delete[] local_data;
        delete[] dest;
    }else if(mmpi.is_master()){
        mmpi.remote(1);
        int slaveSize;
        int * slaveArray;
        mmpi.recv(slaveSize);
        slaveArray = new int[slaveSize];
        mmpi.recv(slaveArray, slaveSize);

//        logArray(local_data, local_data_size,mmpi,"local_data=", "");
//        logArray(slaveArray, slaveSize, mmpi, "salve=", "");

        merge(local_data, local_data_size, slaveArray, slaveSize, global_data);

//        logArray(global_data, global_data_size, mmpi, "sorted_array=", "");

        if (isSorted(global_data, global_data_size)) {
            mmpi << ANSI_COLOR_GREEN << "Array size " << global_data_size << " is sorted" << ANSI_COLOR_RESET
                 << MMPI::endl;
        } else {
            mmpi << ANSI_COLOR_RED << "Array is NOT sorted" << ANSI_COLOR_RESET << MMPI::endl;
        }

        delete[] slaveArray;
        delete[] local_data;
        delete[] global_data;
    }

    cpuTimer.stop();

    mmpi << "Time" << mmpi.cpu_rank() << "=" << cpuTimer.getTime() << "s" << MMPI::endl;

    mmpi.finalize();
    MPI::Finalize();

    exit(EXIT_SUCCESS);
}

void scatterArray(MMPI &mmpi, int *array, std::vector<ThreadPosition> &positions) {
    // Send array parts
    for (int i = 1; i < positions.size(); ++i) {
        mmpi.remote(i);
        mmpi.send(positions[i].size); //Send size of the array so slaves can allocate memory
        mmpi.send(array + positions[i].start, positions[i].size); //Send array
    }
}
