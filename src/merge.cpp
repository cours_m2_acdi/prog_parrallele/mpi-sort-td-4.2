//
// Created by etudiant on 08/11/18.
//

#include <algorithm>
#include <iostream>
#include "merge.h"

void merge(int * firstArray, int firstSize, int * secondArray, int secondSize, int * dest){
    int i = 0;
    int j = 0;
    int k = 0;

    while(i < firstSize and j < secondSize){
        int min;
        if(firstArray[i] < secondArray[j]){
            min = firstArray[i];
            ++i;
        }else{
            min = secondArray[j];
            ++j;
        }
        dest[k] = min;
        ++k;
    }

    for(; i < firstSize; ++i){
        dest[k] = firstArray[i];
        ++k;
    }
    for(; j < secondSize; ++j){
        dest[k] = secondArray[j];
        ++k;
    }
}