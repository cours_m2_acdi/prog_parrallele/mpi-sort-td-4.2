//
// Created by etudiant on 08/11/18.
//

#ifndef MPI_SORT_MERGE_H
#define MPI_SORT_MERGE_H

void merge(int * firstArray, int firstSize, int * secondArray, int secondSize, int * dest);

#endif //MPI_SORT_MERGE_H
