//
// Created by etudiant on 08/11/18.
//

#include "mmpi.h"

#include "mmpi.h"

/**
 * Meta MPI is a wrapper for MPI C++. It simplifies the use
 * of MPI send, receive, gather, scatter functions.
 * For the gather, scatter and reduce functions the processor
 * of rank 0 is considered as the "master" that collects or
 * sends data.
 * This class also acts as a logger so it can be used to print
 * information
 *
 */
void MMPI::find_cpu_name() {
    char name[MPI::MAX_PROCESSOR_NAME];
    int length;

    memset(name, 0, MPI::MAX_PROCESSOR_NAME);
    MPI::Get_processor_name(name, length);
    m_cpu_name = name;
}

void MMPI::init() {
    m_max_cpus = MPI::COMM_WORLD.Get_size();
    m_cpu_rank = MPI::COMM_WORLD.Get_rank();
    find_cpu_name();
    m_pid = getpid();
    tmp_log << "pid=" << m_pid << ", rank=" << m_cpu_rank;
    print_ln();
}

MMPI::MMPI() {
    m_remote_cpu = 0;
    m_message_tag = 0;
    m_verbose = true;
    init();
}


void MMPI::finalize() {
    if (m_cpu_rank == 0) {
        cout.flush();
        cout << std::endl;
        cout << "====================" << std::endl;
        cout << "=== FINAL RESULT ===" << std::endl;
        cout << "====================" << std::endl;
    }
    int in_order = 0;
    while (in_order < m_max_cpus) {
        if (in_order == m_cpu_rank) {
            cout << "---------------------" << std::endl;
            cout << "CPU " << m_cpu_rank << std::endl;
            cout << "---------------------" << std::endl;
            cout << log.str();
            cout.flush();
        }
        ++in_order;
        MPI::COMM_WORLD.Barrier();
    }
}

int MMPI::pid() {
    return m_pid;
}

int MMPI::cpu_rank() {
    return m_cpu_rank;
}

int MMPI::max_cpus() {
    return m_max_cpus;
}

string MMPI::cpu_name() {
    return m_cpu_name;
}

void MMPI::tag(int tag) {
    m_message_tag = tag;
}

void MMPI::remote(int rmt) {
    m_remote_cpu = rmt;
}

void MMPI::barrier() {
    MPI::COMM_WORLD.Barrier();
}

// --------------------------
// output
// --------------------------

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X [%s]", &tstruct);

    return buf;
}

void MMPI::print_ln() {
    string str = currentDateTime();
    log << str << " cpu " << m_cpu_rank << "/" << m_max_cpus << ": " << tmp_log.str() << '\n';
    if (m_verbose) {
        cerr << str << " cpu " << m_cpu_rank << "/" << m_max_cpus << ": " << tmp_log.str() << '\n';
    }
    tmp_log.str("");
}

void MMPI::print(char v) {
    tmp_log << v;
}

void MMPI::print(int v) {
    tmp_log << v;
}

void MMPI::print(string v) {
    tmp_log << v;
}

void MMPI::print(float v) {
    tmp_log << v;
}

void MMPI::print(double v) {
    tmp_log << v;
}