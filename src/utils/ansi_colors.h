//
// Created by etudiant on 08/11/18.
//

#ifndef MPI_SORT_AINSI_COLORS_H
#define MPI_SORT_AINSI_COLORS_H


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#endif //MPI_SORT_AINSI_COLORS_H
