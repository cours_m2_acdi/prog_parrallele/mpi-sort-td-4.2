//
// Created by Paulin Violette on 08/11/18.
//

#ifndef MPI_SORT_UTILS_H
#define MPI_SORT_UTILS_H

#include "../mmpi/mmpi.h"

bool isSorted(const int * array, int size);

void populateArrayRandom(int *array, int size);

void logArray(int *array, int size, MMPI &mmpi, const char *prefix, const char *suffix);
#endif //MPI_SORT_UTILS_H
